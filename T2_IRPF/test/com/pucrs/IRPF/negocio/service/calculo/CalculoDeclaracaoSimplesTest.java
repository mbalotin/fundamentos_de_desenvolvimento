/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pucrs.IRPF.negocio.service.calculo;

import com.pucrs.IRPF.negocio.to.CalculoBucket;
import com.pucrs.IRPF.negocio.to.ClienteBucket;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 15280212
 */
public class CalculoDeclaracaoSimplesTest {
    
    ClienteBucket cliente;
    CalculoDeclaracaoSimples instance;

    @Before
    public void setUp() {
        cliente = new ClienteBucket();
        instance = new CalculoDeclaracaoSimples();
    }
    
    
    @Test
    public void testImpostoPagarBase10000() {
        cliente.setTotRendimentos(10000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(0, result.getImpostoPagar(), 0.009f);
    }
    
    @Test
    public void testImpostoPagarBase15000() {
        cliente.setTotRendimentos(15000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(2137.5f, result.getImpostoPagar(), 0.009f);
    }
    
    @Test
    public void testImpostoPagarBase50000() {
        cliente.setTotRendimentos(50000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(13062.5, result.getImpostoPagar(), 0.009f);
    }
}
