/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pucrs.IRPF.negocio.service.calculo;

import com.pucrs.IRPF.negocio.to.CalculoBucket;
import com.pucrs.IRPF.negocio.to.ClienteBucket;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 15280212
 */
public class CalculoDeclaracaoCompletaTest {

    ClienteBucket cliente;
    CalculoDeclaracaoCompleta instance;

    @Before
    public void setUp() {
        cliente = new ClienteBucket();
        instance = new CalculoDeclaracaoCompleta();
    }


    @Test
    public void testDescBase50anos2Dependentes() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(2);
        float result = instance.calcBasePercentual(cliente);
        assertEquals(0.98f, result, 0.009f);
    }
    
    @Test
    public void testDescBase50anos4Dependentes() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(4);
        float result = instance.calcBasePercentual(cliente);
        assertEquals(0.965f, result, 0.009f);
    }
    
    @Test
    public void testDescBase50anos10Dependentes() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        float result = instance.calcBasePercentual(cliente);
        assertEquals(0.95f, result, 0.009f);
    }
    
    
    @Test
    public void testDescBase70anos2Dependentes() {
        cliente.setIdade(70);
        cliente.setQtdeDependentes(2);
        float result = instance.calcBasePercentual(cliente);
        assertEquals(0.97f, result, 0.009f);
    }
    
    @Test
    public void testDescBase70anos4Dependentes() {
        cliente.setIdade(70);
        cliente.setQtdeDependentes(4);
        float result = instance.calcBasePercentual(cliente);
        assertEquals(0.955f, result, 0.009f);
    }
    
    @Test
    public void testDescBase70anos10Dependentes() {
        
        cliente.setIdade(70);
        cliente.setQtdeDependentes(10);
        float result = instance.calcBasePercentual(cliente);
        assertEquals(0.94f, result, 0.009f);
    }
    
    
    @Test
    public void testBase10000() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        cliente.setContrib(0);
        cliente.setTotRendimentos(10000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(9500, result.getBaseCalculo(), 0.009f);
    }
    
    @Test
    public void testBase15000() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        cliente.setContrib(0);
        cliente.setTotRendimentos(15000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(14250, result.getBaseCalculo(), 0.009f);
    }
    
    @Test
    public void testBase50000() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        cliente.setContrib(0);
        cliente.setTotRendimentos(50000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(47500, result.getBaseCalculo(), 0.009f);
    }
    
    
    @Test
    public void testImpostoPagarBase10000() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        cliente.setContrib(0);
        cliente.setTotRendimentos(10000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(0, result.getImpostoPagar(), 0.009f);
    }
    
    @Test
    public void testImpostoPagarBase15000() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        cliente.setContrib(0);
        cliente.setTotRendimentos(15000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(2137.5f, result.getImpostoPagar(), 0.009f);
    }
    
    @Test
    public void testImpostoPagarBase50000() {
        cliente.setIdade(50);
        cliente.setQtdeDependentes(10);
        cliente.setContrib(0);
        cliente.setTotRendimentos(50000);
        CalculoBucket result = instance.calcularImposto(cliente);
        assertEquals(13062.5, result.getImpostoPagar(), 0.009f);
    }
    

}
