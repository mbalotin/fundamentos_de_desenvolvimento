/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pucrs.IRPF.negocio.service;

import com.pucrs.IRPF.negocio.to.CalculoBucket;
import com.pucrs.IRPF.negocio.to.ClienteBucket;

/**
 *
 * @author 15280212
 */
public class CalculoIRPFService {
    public CalculoBucket calcularImposto(ClienteBucket cliente){
        return cliente.getTipoCalculo().getCalculo().calcularImposto(cliente);
    }
}
