/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pucrs.IRPF.negocio.service.calculo;

import com.pucrs.IRPF.negocio.to.CalculoBucket;
import com.pucrs.IRPF.negocio.to.ClienteBucket;

/**
 *
 * @author 15280212
 */
public class CalculoDeclaracaoSimples implements CalculoImposto{

    @Override
    public CalculoBucket calcularImposto(ClienteBucket cliente) {
        CalculoBucket result = new CalculoBucket();
        result.setBaseCalculo(cliente.getTotRendimentos() - cliente.getContrib());
        
        result.setBaseCalculo(result.getBaseCalculo() * 0.95f);

        if (result.getBaseCalculo() >= 12000){
            if (result.getBaseCalculo() < 24000){
                result.setImpostoPagar(result.getBaseCalculo() * 0.15f);
            }else{
                result.setImpostoPagar(result.getBaseCalculo() * 0.275f);
            }
        }
        
        return result;
    }
    
}
