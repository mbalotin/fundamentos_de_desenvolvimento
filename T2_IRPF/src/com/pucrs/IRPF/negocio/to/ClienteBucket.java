package com.pucrs.IRPF.negocio.to;

public class ClienteBucket {

    private String nome;
    private String cpf;
    private int idade;
    private int qtdeDependentes;
    private float contrib;
    private float totRendimentos;
    private TipoCalculoEnum tipoCalculo;

    public ClienteBucket() {
        nome = "";
        cpf = "";
        idade = 0;
        qtdeDependentes = 0;
        contrib = 0f;
        totRendimentos = 0;
    }

    public TipoCalculoEnum getTipoCalculo() {
        return tipoCalculo;
    }

    public void setTipoCalculo(TipoCalculoEnum tipoCalculo) {
        this.tipoCalculo = tipoCalculo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getQtdeDependentes() {
        return qtdeDependentes;
    }

    public void setQtdeDependentes(int qtdeDependentes) {
        this.qtdeDependentes = qtdeDependentes;
    }

    public float getContrib() {
        return contrib;
    }

    public void setContrib(float contrib) {
        this.contrib = contrib;
    }

    public float getTotRendimentos() {
        return totRendimentos;
    }

    public void setTotRendimentos(float totRendimentos) {
        this.totRendimentos = totRendimentos;
    }
}
