/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pucrs.IRPF.negocio.to;

/**
 *
 * @author 15280212
 */
public class CalculoBucket {

    private float baseCalculo;
    private float impostoPagar;

    public CalculoBucket() {
        baseCalculo = 0;
        impostoPagar = 0;
    }
 
    /**
     * @return the baseCalculo
     */
    public float getBaseCalculo() {
        return baseCalculo;
    }

    /**
     * @param baseCalculo the baseCalculo to set
     */
    public void setBaseCalculo(float baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    /**
     * @return the impostoPagar
     */
    public float getImpostoPagar() {
        return impostoPagar;
    }

    /**
     * @param impostoPagar the impostoPagar to set
     */
    public void setImpostoPagar(float impostoPagar) {
        this.impostoPagar = impostoPagar;
    }
}
