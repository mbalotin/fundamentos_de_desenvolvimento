package com.pucrs.IRPF.negocio.to;

import com.pucrs.IRPF.negocio.service.calculo.CalculoDeclaracaoCompleta;
import com.pucrs.IRPF.negocio.service.calculo.CalculoDeclaracaoSimples;
import com.pucrs.IRPF.negocio.service.calculo.CalculoImposto;

public enum TipoCalculoEnum {
    SIMPLES(new CalculoDeclaracaoSimples()), COMPLETO(new CalculoDeclaracaoCompleta());
    
    private final CalculoImposto calculo;
    
    private TipoCalculoEnum(CalculoImposto calculo) {
        this.calculo = calculo;
    }

    /**
     * @return the calculo
     */
    public CalculoImposto getCalculo() {
        return calculo;
    }
    
    
}
