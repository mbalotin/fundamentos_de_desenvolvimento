package com.pucrs.IRPF.ui;

import com.pucrs.IRPF.negocio.service.CalculoIRPFService;
import com.pucrs.IRPF.negocio.to.CalculoBucket;
import com.pucrs.IRPF.negocio.to.ClienteBucket;
import com.pucrs.IRPF.negocio.to.TipoCalculoEnum;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CalculoInterface extends Application {

    private final Label LABEL_TIPO = new Label("Tipo de Declaração:");
    private final Label LABEL_NOME = new Label("Nome*:");
    private final Label LABEL_CPF = new Label("CPF*:");
    private final Label LABEL_IDADE = new Label("Idade**:");
    private final Label LABEL_DEP = new Label("Número de dependentes**:");
    private final Label LABEL_CONTRIB = new Label("Contibuição previdenciária Oficial**:");
    private final Label LABEL_TOT_RND = new Label("Total de Rendimentos*:");

    private TextField nomeTF = new TextField();
    private MaskTextField cpfTF = new MaskTextField("", "NNNNNNNNNNN");
    private MaskTextField idadeTF = new MaskTextField("", "NN");
    private MaskTextField depTF = new MaskTextField("", "NN");
    private MaskTextField contribTF = new MaskTextField("", "NN!.NN");
    private MaskTextField totRendimentosTF = new MaskTextField("", "NN!.NN");
    private Button confirmButton = new Button("Calcular");

    private final ToggleGroup group = new ToggleGroup();
    private RadioButton dclCompleta = new RadioButton("Completa");
    private RadioButton dclSimpl = new RadioButton("Simplificada");

    private ClienteBucket cliente;

    private CalculoIRPFService service;

    @Override
    public void start(Stage primaryStage) throws Exception {
        cliente = new ClienteBucket();
        service = new CalculoIRPFService();
        GridPane formPane = new GridPane();
        formPane.setAlignment(Pos.CENTER);
        formPane.setHgap(10);
        formPane.setVgap(10);
        formPane.setPadding(new Insets(25, 25, 25, 25));
        int row = 0;
        Text scenetitle = new Text("Dados para declaração");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 16));
        formPane.add(scenetitle, 0, row++, 2, 1);

        bindEvents();
        bindRadioButtonGroup();

        formPane.add(LABEL_TIPO, 0, row);
        formPane.add(dclSimpl, 1, row++);
        formPane.add(dclCompleta, 1, row++);
        formPane.add(LABEL_NOME, 0, row);
        formPane.add(nomeTF, 1, row++);
        formPane.add(LABEL_CPF, 0, row);
        formPane.add(cpfTF, 1, row++);
        formPane.add(LABEL_IDADE, 0, row);
        formPane.add(idadeTF, 1, row++);
        formPane.add(LABEL_DEP, 0, row);
        formPane.add(depTF, 1, row++);
        formPane.add(LABEL_CONTRIB, 0, row);
        formPane.add(contribTF, 1, row++);
        formPane.add(LABEL_TOT_RND, 0, row);
        formPane.add(totRendimentosTF, 1, row++);

        Text sceneObservations = new Text("* Obrigatório\n** Obrigatório para declaração completa.");
        sceneObservations.setFont(Font.font("Tahoma", FontWeight.NORMAL, 10));
        formPane.add(sceneObservations, 0, row++, 2, 1);

        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(confirmButton);
        formPane.add(hbBtn, 0, row++, 2, 1);

        Scene scene = new Scene(formPane, 600, 400);

        primaryStage.setTitle("IRPF - PUCRS - Márcio Balotin");
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private void bindRadioButtonGroup() {

        dclSimpl.setToggleGroup(group);
        dclSimpl.setSelected(true);
        cliente.setTipoCalculo(TipoCalculoEnum.SIMPLES);
        dclSimpl.setUserData(TipoCalculoEnum.SIMPLES);
        dclCompleta.setToggleGroup(group);
        dclCompleta.setUserData(TipoCalculoEnum.COMPLETO);
    }

    private void bindEvents() {
        nomeTF.textProperty().addListener((obs, old, value) -> {
            cliente.setNome(value);
        });
        cpfTF.textProperty().addListener((obs, old, value) -> {
            cliente.setCpf(value);
        });
        idadeTF.textProperty().addListener((obs, old, value) -> {
            cliente.setIdade(Integer.parseInt(value));
        });
        depTF.textProperty().addListener((obs, old, value) -> {
            cliente.setQtdeDependentes(Integer.parseInt(value));
        });
        contribTF.textProperty().addListener((obs, old, value) -> {
            cliente.setContrib(Float.parseFloat(value));
        });
        totRendimentosTF.textProperty().addListener((obs, old, value) -> {
            cliente.setTotRendimentos(Float.parseFloat(value));
        });
        group.selectedToggleProperty().addListener((obs, old, value) -> {
            cliente.setTipoCalculo((TipoCalculoEnum) group.getSelectedToggle().getUserData());
        });

        confirmButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (isValid(cliente)) {
                    showCalculo(service.calcularImposto(cliente));
                }
            }

        });
    }

    private void showCalculo(CalculoBucket calculo) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Imposto devido");
        alert.setHeaderText("Imposto devido");
        alert.setContentText("Base de Calculo: R$ " + String.format("%.2f", calculo.getBaseCalculo()) + " \n Imposto a pagar: R$ " + String.format("%.2f", calculo.getImpostoPagar()));

        alert.showAndWait();

    }

    private boolean isValid(ClienteBucket cliente) {
        StringBuilder errors = new StringBuilder();

        if (cliente.getNome().isEmpty()) {
            errors.append("Nome é um campo obrigatório \n");
        }
        if (cliente.getCpf().isEmpty()) {
            errors.append("CPF é um campo obrigatório \n");
        }

        if (TipoCalculoEnum.COMPLETO.equals(cliente.getTipoCalculo())) {
            if (cliente.getIdade() == 0) {
                errors.append("Idade é um campo obrigatório para declarações Completas\n");
            }
            if (cliente.getQtdeDependentes() == 0) {
                errors.append("Numero de Dependentes é um campo obrigatório para declarações Completas\n");
            }
        }

        if (cliente.getContrib() == 0) {
            errors.append("Contribuição é um campo obrigatório\n");
        }
        if (cliente.getTotRendimentos() == 0) {
            errors.append("Total de rendimentos é um campo obrigatório\n");
        }

        if (errors.toString().isEmpty()) {
            return true;
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Atenção");
            alert.setHeaderText("Informações obrigatórias");
            alert.setContentText(errors.toString());
            alert.showAndWait();
            return false;
        }

    }

}
