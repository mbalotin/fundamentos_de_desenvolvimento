package application;

import com.pucrs.IRPF.ui.CalculoInterface;

import javafx.application.Application;

public class Main {
	public static void main(String[] args) {
		Application.launch(CalculoInterface.class, args);
	}
}